﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine.ParticleSystemJobs;

// thing to hold the "where" a particle should be on the mesh.
public struct TrianglePoint
{
	public int triangleIndex;
	public float3 barycentricCoordinate;

}

// Not actually using Unity's C# Job System yet.
// I was intending on using the Job System afterwards,
// but it worked fine without it.

public class FollowMeshParticleJob : MonoBehaviour
{
	public SkinnedMeshRenderer skinnedMeshRenderer;
	public int particleCount = 100;

	private Mesh mesh;

	public ParticleSystem ps;
	private ParticleSystem.Particle[] particles;
	private TrianglePoint[] trianglePoints;

	public List<int> triangles;
	public List<Vector3> vertices;
	public Color color;
	public float particleSpeed = 2.0f;
	public float particleRotationLerpRate = 5.0f;

	// Start is called before the first frame update
	void Start()
	{
		mesh = new Mesh();
		// generate
		skinnedMeshRenderer.BakeMesh(mesh);

		triangles = new List<int>(mesh.triangles);
		vertices = new List<Vector3>(mesh.vertices);

		particles = new ParticleSystem.Particle[particleCount];
		trianglePoints = new TrianglePoint[particleCount];


		int triangleCount = triangles.Count / 3;

		Matrix4x4 localToWorld = skinnedMeshRenderer.transform.localToWorldMatrix;

		for (int i = 0; i < particleCount; i++)
		{
			ParticleSystem.Particle p = new ParticleSystem.Particle();
			TrianglePoint t = new TrianglePoint();

			t.triangleIndex = UnityEngine.Random.Range(0, triangleCount);
			t.barycentricCoordinate = RandomBarycentricCoordinate();

			p.position = localToWorld.MultiplyPoint3x4(TrianglePointPositionInMesh(t));
			p.remainingLifetime = p.startLifetime = 10000;
			p.startColor = color;
			//p.startColor = UnityEngine.Random.ColorHSV(0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f);
			p.velocity = UnityEngine.Random.onUnitSphere * particleSpeed;
			p.startSize = 0.02f;

			trianglePoints[i] = t;
			particles[i] = p;
		}

		ps.SetParticles(particles, particleCount);
	}

	private float3 TrianglePointPositionInMesh(TrianglePoint t)
	{
		float3 pos;

		int baseIndex = t.triangleIndex * 3;

		pos = vertices[triangles[baseIndex]] * t.barycentricCoordinate.x
			+ vertices[triangles[baseIndex + 1]] * t.barycentricCoordinate.y
			+ vertices[triangles[baseIndex + 2]] * t.barycentricCoordinate.z;

		return pos;

	}

	static float3 RandomBarycentricCoordinate()
	{
		float3 value = new float3(UnityEngine.Random.onUnitSphere);

		float total = value.x + value.y + value.z;
		value /= total;

		return value;
	}

	// Update is called once per frame
	void Update()
	{
		Vector3 forward = new Vector3(0, 0, 1.0f);

		// refresh the triangles and meshes
		skinnedMeshRenderer.BakeMesh(mesh);
		triangles = new List<int>(mesh.triangles);
		vertices = new List<Vector3>(mesh.vertices);
		float deltaTime = Time.deltaTime;
		Matrix4x4 localToWorld = skinnedMeshRenderer.transform.localToWorldMatrix;

		for (int i = 0; i < particleCount; i++)
		{
			TrianglePoint t = trianglePoints[i];
			ParticleSystem.Particle p = particles[i];

			float3 targetPosition = localToWorld.MultiplyPoint3x4(TrianglePointPositionInMesh(t));
			float3 pos = p.position;

			float3 diff = targetPosition - pos;
			float3 dir = math.lerp(math.normalize(diff), UnityEngine.Random.onUnitSphere, UnityEngine.Random.value * 0.5f);
			float dist = math.clamp((math.length(diff) / 0.1f), 0.0f, 1.0f);

			Quaternion targetRotation = Quaternion.LookRotation(dir);
			Quaternion rotation = Quaternion.LookRotation(math.normalize(p.velocity));

			rotation = Quaternion.Slerp(rotation, targetRotation, deltaTime * particleRotationLerpRate);
			p.velocity = rotation * forward * dist;
			p.position += p.velocity * deltaTime;

			particles[i] = p;
		}

		ps.SetParticles(particles);
	}
}
